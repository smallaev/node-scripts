/**
 * A command line script to convert images using imagemagick.
 * 
 * Usage:
 * cd /path/to/directory/containing/this/file
 * npm i
 * node index.js convert -path /path/to/images/directory -format [png|jpg|...] -quality [0-100]
 * 
 * Requires: 
 * Node.js 10.x+
 * Imagemagick package
 * 
 */

const yargs = require('yargs');
const fsp = require('fs').promises;
const path = require('path');
const { exec } = require('child_process');
const { exit } = require('process');

const argv = yargs
    .command('convert', 'Convert images', {
        path: {
            description: 'path to the file or the folder to convert',
            alias: 'f',
            type: 'string',
            required: true,
        },
        format: {
            description: 'format to convert to',
            alias: 't',
            type: 'string',
            required: true,
        },
        quality: {
            description: 'compression quality',
            alias: 'q',
            type: 'number',
            default: 90,
        }
    }).argv;

if (!argv.path) {
    console.error('Please specify the path to the file(s)');
    return;
}

// checking if the supplied path is a folder
isDirectory = async (path) => {
    try {
        const stats = await fsp.stat(path);
        return stats.isDirectory();
    } catch (e) {
        console.error(e);
        exit();
    }
}

// goes over the folders recursively and coverts all files
traverseAndConvert = async (fPath) => {
    try {
        if (isDirectory(fPath)) {
            const files = await fsp.readdir(fPath);
            // looping over the files/folders within current folder 
            for (let i = 0; i < files.length; i++) {
                const file = files[i];
                const fullPath = path.join(fPath, file);
                if (await isDirectory(fullPath)) {
                    console.log(`Directory ${fullPath}`);
                    await traverseAndConvert(fullPath);
                } else {
                    // if the current path refers to a file, converting the file
                    await convertFile(fullPath);
                }
            }
        } else {
            await convertFile(fPath);
        }
        
    } catch (e) {
        console.error(e);
        exit();
    }    
}

// Promisifying the child_process.exec function to be able to await on it
execCmd = (command) => {
    return new Promise((resolve, reject) => {
        exec(command, (err, stdout, stderr) => {
            if (err) {
                reject(err);
            }
            if (stderr) {
                reject(stderr);
            }
            resolve();
        });
    });
}

convertFile = async (file) => {
    // the path of the converted file
    const newFile = `${file.split('.')[0]}.${argv.format}`;
    // the conversion command
    const command = `convert -quality ${argv.quality} '${file}' '${newFile}'`;
    // making sure we are not trying to convert files that are already converted - ie image.png into image.png
    // this is important since we are not currently filtering out files by their extensions - we just get all files and try to convert them all.
    if (file !== newFile) {          
        try {
            await execCmd(command);
            console.log(`File ${file} converted successfully`);
        } catch (e) {
            console.error(e);
            exit();
        }
    }
};

traverseAndConvert(argv.path);
